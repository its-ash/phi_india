import uuid

from django.db import models


class ContactUsModel(models.Model):
    class Meta:
        verbose_name = 'Contact Us'
        verbose_name_plural = 'Contact Us'
    
    id = models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True)
    name = models.CharField(max_length=100)
    email = models.CharField(max_length=20)
    phone = models.CharField(max_length=10)
    message = models.TextField(max_length=1000)
    submit_at = models.DateTimeField(auto_now_add=True, editable=False)


class Quotes(models.Model):
    class Meta:
        verbose_name = 'Quote'
        verbose_name_plural = 'Quotes'
    
    uuid = models.UUIDField(default=uuid.uuid4, editable=False)
    sanskrit = models.TextField(max_length=1000)
    english = models.TextField(max_length=1000)
    date = models.DateTimeField(auto_now_add=True)
    
    def __str__(self):
        return self.sanskrit
    
    def __repr__(self):
        return self.__str__()
