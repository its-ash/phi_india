import uuid
from datetime import time

from django.db import models

from course.models import Course


class Review(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    name = models.CharField(max_length=100)
    review = models.TextField(max_length=1000)
    rating = models.IntegerField()
    email = models.CharField(max_length=100)
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='reviews')
    date = models.DateTimeField(auto_now_add=True, editable=False)
    verified = models.BooleanField(default=False)
    
    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.__str__()
