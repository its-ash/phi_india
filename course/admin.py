from django.contrib import admin

from course.forms import CourseForm
from course.models import Course, Objective, CourseCategory
from course.models import CourseBasicPack, CourseIntermediatePack, CourseExpertPack


class ObjectiveAdmin(admin.TabularInline):
    model = Objective


class CourseBasicPackAdmin(admin.TabularInline):
    model = CourseBasicPack


class CourseIntermediatePackAdmin(admin.TabularInline):
    model = CourseIntermediatePack


class CourseExpertPackAdmin(admin.TabularInline):
    model = CourseExpertPack


@admin.register(Course)
class CourseAdmin(admin.ModelAdmin):
    form = CourseForm
    list_filter = ('created', 'category')
    list_display = ('name', 'teacher', 'level')
    search_fields = ('name', 'cost', 'teacher', 'requirements', 'learns')
    readonly_fields = ('slug', 'created')
    fields = (('name',),
              ('basic_cost', 'intermediate_cost', 'expert_cost'),
              ('pdf',),
              ('image', 'cover'),
              ('summary',),
              ('duration', 'lessons'),
              ('level', 'teacher', 'category'),
              ('syllabus',),
              ('requirements',),
              ('learn',),
              ('created', 'slug',))
    inlines = [ObjectiveAdmin, CourseBasicPackAdmin, CourseIntermediatePackAdmin, CourseExpertPackAdmin]


@admin.register(CourseCategory)
class CourseCategory(admin.ModelAdmin):
    readonly_fields = ('id',)
    list_display = ('name', 'id')
    search_fields = ('name',)
