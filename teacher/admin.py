from django.contrib import admin

from teacher.models import Skill, Teachers


class SkillAdmin(admin.TabularInline):
    model = Skill


@admin.register(Teachers)
class TeacherAdmin(admin.ModelAdmin):
    list_display = ('name', 'phone', 'email')
    search_fields = ('name', 'email')
    list_filter = ('name',)
    readonly_fields = ('id', 'slug')
    fields = (('id', 'slug'),
              ('name', 'phone'),
              ('email', 'subject'),
              ('photo',),
              ('address',),
              ('detail',),
              ('biography',)
              )
    inlines = [SkillAdmin]
