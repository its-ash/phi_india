from django import forms

from contact.models import ContactUsModel


class ContactUsForm(forms.ModelForm):
    class Meta:
        model = ContactUsModel
        fields = "__all__"
    
    def __init__(self, *args, **kwargs):
        super(ContactUsForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['id'] = 'name'
        self.fields['email'].widget.attrs['id'] = 'email'
        self.fields['phone'].widget.attrs['id'] = 'tel-number'
        self.fields['message'].widget.attrs['id'] = 'comment'
