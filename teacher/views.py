from django.urls import reverse
from django.views.generic import ListView, DetailView
from django.views.generic.edit import FormMixin

from teacher.forms import TeacherMessageForm
from teacher.models import Teachers


class TeachersListView(ListView):
    model = Teachers
    template_name = 'Teacher.html'
    context_object_name = 'teachers'
    
    def get_queryset(self):
        _query_set = Teachers.objects.all()
        _batch_query_set = []
        # Create Batch For 4*4 Display
        for batch in range(0, _query_set.count(), 4):
            _batch_query_set.append(_query_set[batch:batch + 4])
        return _batch_query_set
    
    def get_context_data(self, *args, **kwargs):
        kwargs.update({'submitted': 'display'})
        return super(TeachersListView, self).get_context_data(*args, **kwargs)


class TeacherDetailView(FormMixin, DetailView):
    form_class = TeacherMessageForm
    model = Teachers
    template_name = 'TeacherDetail.html'
    context_object_name = 'teacher'
    
    def get_success_url(self):
        url_param = {
            'done': 'done',
            'slug': self.kwargs.get('slug', None)
        }
        return reverse('teacher_detail', kwargs=url_param)
    
    def get_initial(self):
        _initial = super(TeacherDetailView, self).get_initial()
        _initial.update({'teacher': self.get_object().id})
        return _initial
    
    def get_context_data(self, **kwargs):
        _context = super(TeacherDetailView, self).get_context_data(**kwargs)
        _context.update(self.kwargs)
        return _context
    
    def form_valid(self, form):
        form.save()
        return super(TeacherDetailView, self).form_valid(form)
    
    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)
