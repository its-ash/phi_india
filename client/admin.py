from django.contrib import admin

from client.models import UserData


@admin.register(UserData)
class UserDataAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'course_name', 'phone', 'course_mode', 'payment')
    list_filter = ('date', 'payment')
    search_fields = ('first_name', 'last_name', 'address', 'city')
    
    def has_change_permission(self, request, obj=None):
        return False
    
    def has_add_permission(self, request):
        return False
