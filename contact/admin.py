from django.contrib import admin

from contact.models import ContactUsModel, Quotes


@admin.register(ContactUsModel)
class ContactUsAdmin(admin.ModelAdmin):
    list_filter = ('submit_at',)
    list_display = ('name', 'email', 'phone', 'message')
    search_fields = ('name', 'message', 'email')
    
    def has_add_permission(self, request):
        return False
    
    def has_change_permission(self, request, obj=None):
        return False


@admin.register(Quotes)
class ContactUsAdmin(admin.ModelAdmin):
    readonly_fields = ('uuid', 'date')
    list_filter = ('date',)
    list_display = ('sanskrit', 'english')
    search_fields = ('english',)
