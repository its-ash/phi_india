from django import forms
from django.forms.widgets import HiddenInput

from teacher.models import Message


class TeacherMessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = "__all__"
    
    def __init__(self, *args, **kwargs):
        super(TeacherMessageForm, self).__init__(*args, **kwargs)
        self.fields['teacher'].widget = HiddenInput()
