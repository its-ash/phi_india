import uuid

from django.db import models
from django.utils.text import slugify


class Teachers(models.Model):
    class Meta:
        verbose_name = 'Teacher'
        verbose_name_plural = 'Teachers'
    
    id = models.UUIDField(default=uuid.uuid4, primary_key=True)
    subject = models.CharField(max_length=100)
    photo = models.ImageField(upload_to='teacher_image')
    name = models.CharField(max_length=100)
    education = models.TextField(max_length=500)
    address = models.CharField(max_length=100)
    email = models.EmailField()
    phone = models.IntegerField()
    biography = models.TextField(max_length=1000)
    slug = models.SlugField(default="default-slug")
    
    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        return super(Teachers, self).save(*args, **kwargs)
    
    @property
    def skills(self):
        _data_list = self.skill.all()
        return _data_list
    
    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.__str__()


class Skill(models.Model):
    class Meta:
        verbose_name = 'Skill'
        verbose_name_plural = 'Skills'
    
    id = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)
    name = models.CharField(max_length=100, verbose_name="Skill Name")
    points = models.IntegerField(help_text="Rate 1% to 100%")
    teacher = models.ForeignKey(Teachers, related_name='skill', on_delete=models.CASCADE)
    
    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.__str__()


class Message(models.Model):
    class Meta:
        verbose_name = 'Message'
        verbose_name_plural = 'Messages'
    
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True)
    name = models.CharField(max_length=100)
    email = models.EmailField()
    message = models.TextField(max_length=2000)
    teacher = models.ForeignKey(Teachers, on_delete=models.CASCADE, related_name='messages')
