from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, re_path

from about.views import AboutUsView
from app.views import Homepage
from contact.views import ContactUsView, ThanksView
from course.views import CourseListView, CourseGridView, CourseDetailView, CourseCategoryView
from course.views import CourseSelectionListView, CourseRegistrationView
from phi_india import settings
from teacher.views import TeachersListView, TeacherDetailView

admin.site.site_header = 'Phi - India'
admin.site.site_title = 'Phi - India'
admin.site.site_url = 'http://phiindia.in/'
admin.site.index_title = 'Phi-India administration'
admin.empty_value_display = '**Empty**'

urlpatterns = [
    path('', Homepage.as_view(), name="homepage"),
    path('teacher/', TeachersListView.as_view(), name="teacher_list"),
    re_path('teacher-detail/(?P<slug>[-a-zA-Z0-9_]+)/(?P<done>[a-z]*)/',
            TeacherDetailView.as_view(), name='teacher_detail'),
    path('about', AboutUsView.as_view(), name='about_us'),
    path('contact-us', ContactUsView.as_view(), name='contact_us'),
    path('thanks', ThanksView.as_view(), name='thanks'),
    path('course-list/', CourseListView.as_view(), name='course_list'),
    path('course-grid/', CourseGridView.as_view(), name='course_grid'),
    path('course-category-list/<slug:slug>', CourseCategoryView.as_view(), name='course_category'),
    path('course-detail/<slug:slug>', CourseDetailView.as_view(), name='course_detail'),
    path('course-select/<slug:slug>', CourseSelectionListView.as_view(), name='course_price_select'),
    path('course-registartion/<slug:slug>/<slug:plan>', CourseRegistrationView.as_view(), name='course_registration'),
    path('admin/', admin.site.urls),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
