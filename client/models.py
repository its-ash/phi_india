import uuid

from django.db import models

from course.models import Course


class UserData(models.Model):
    class Meta:
        verbose_name = 'User'
        verbose_name_plural = "Users"
    
    uuid = models.UUIDField(default=uuid.uuid4, primary_key=True, editable=False)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    address = models.CharField(max_length=100)
    city = models.CharField(max_length=20)
    state = models.CharField(max_length=20)
    phone = models.CharField(max_length=12)
    email = models.CharField(max_length=50)
    additional = models.TextField(max_length=500, blank=True)
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='user')
    date = models.DateTimeField(auto_now_add=True, editable=False)
    course_mode = models.CharField(max_length=100)
    payment = models.IntegerField(default=0, blank=True)
    
    @property
    def course_name(self):
        return self.course.name
    
    def __str__(self):
        return " ".join([self.first_name, self.last_name])
    
    def __repr__(self):
        return self.__str__()
