from django.urls import reverse
from django.views.generic import FormView, TemplateView

from contact.forms import ContactUsForm
from contact.models import Quotes
from course.models import Course


class ContactUsView(FormView):
    form_class = ContactUsForm
    template_name = 'Contact.html'
    
    def get_success_url(self):
        return reverse('thanks')
    
    def form_valid(self, form):
        form.save()
        return super(ContactUsView, self).form_valid(form)


class ThanksView(TemplateView):
    template_name = "Thanks.html"
    
    def get_context_data(self, **kwargs):
        _quotes = Quotes.objects.order_by("?").first()
        _context = super(ThanksView, self).get_context_data(**kwargs)
        _context.update({"courses": Course.objects.order_by("?")[:4]})
        _context.update({'quote': _quotes})
        return _context
