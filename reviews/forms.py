from django import forms
from django.forms import HiddenInput

from reviews.models import Review


class ReviewForm(forms.ModelForm):
    rating_choice = ((5, 'Perfect'),
                     (4, 'Good'),
                     (3, 'Average'),
                     (2, 'Not that bad',),
                     (1, 'Very Poor',))
    
    class Meta:
        model = Review
        fields = "__all__"
    
    def __init__(self, *args, **kwargs):
        super(ReviewForm, self).__init__(*args, **kwargs)
        self.fields['course'].widget = HiddenInput()
        self.fields['email'].widget.attrs['type'] = 'email'
    
    rating = forms.ChoiceField(choices=rating_choice)
