from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import ListView, DetailView
from django.views.generic.edit import FormMixin

from client.forms import UserRegistrationForm
from course.models import Course, CourseCategory
from reviews.forms import ReviewForm


class CourseListView(ListView):
    template_name = 'CourseList.html'
    model = Course
    context_object_name = 'courses'
    
    def get_context_data(self, *args, **kwargs):
        _data = super(CourseListView, self).get_context_data(*args, **kwargs)
        _data.update({'mode': 'list'})
        _data.update({'category': CourseCategory.objects.all()})
        return _data


class CourseGridView(ListView):
    template_name = 'CourseList.html'
    model = Course
    context_object_name = 'courses'
    
    def get_context_data(self, *args, **kwargs):
        _data = super(CourseGridView, self).get_context_data(*args, **kwargs)
        _data.update({'mode': 'grid'})
        _data.update({'category': CourseCategory.objects.all()})
        return _data


class CourseCategoryView(ListView):
    template_name = 'CourseList.html'
    model = Course
    context_object_name = 'courses'
    
    def get_queryset(self):
        return Course.objects.filter(category__slug=self.kwargs.get('slug'))
    
    def get_context_data(self, *args, **kwargs):
        _data = super(CourseCategoryView, self).get_context_data(*args, **kwargs)
        _data.update({'category': CourseCategory.objects.all()})
        _data.update({'mode': 'category'})
        _data.update({'random_course_list': Course.objects.order_by('?')[:5]})
        return _data


class CourseDetailView(FormMixin, DetailView):
    template_name = 'CourseDetail.html'
    context_object_name = 'course'
    model = Course
    slug = None
    form_class = ReviewForm
    
    def get_success_url(self):
        return reverse("thanks")
    
    def get_initial(self):
        _initial = super(CourseDetailView, self).get_initial()
        _initial.update({'course': self.get_object().id})
        return _initial
    
    def form_valid(self, form):
        form.save()
        return super(CourseDetailView, self).form_valid(form)
    
    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        return HttpResponseRedirect(self.get_success_url())


class CourseSelectionListView(DetailView):
    template_name = 'PriceSelection.html'
    model = Course
    context_object_name = 'course'


class CourseRegistrationView(FormMixin, DetailView):
    template_name = 'CheckOut.html'
    model = Course
    form_class = UserRegistrationForm
    context_object_name = 'course'
    slug_field = 'slug'
    
    def get_success_url(self):
        return reverse("thanks")
    
    def get_context_data(self, **kwargs):
        _context = super(CourseRegistrationView, self).get_context_data(**kwargs)
        _plan = _context.get('plan', 'basic')
        _course = self.get_object()
        _price = 0
        if _plan == 'expert':
            _price = _course.expert_cost
        elif _plan == 'intermediate':
            _price = _course.intermediate_cost
        else:
            _price = _course.basic_cost
        _tax = (_price * 12) / 100
        _total = _tax + _price
        _context.update({'price': _price, 'tax': _tax, 'total': _total})
        return _context
    
    def get_initial(self):
        _initial = super(CourseRegistrationView, self).get_initial()
        _initial.update({'course': self.get_object().id, 'course_mode': self.kwargs.get('plan', 'basic')})
        return _initial
    
    def form_invalid(self, form):
        print(form.errors)
    
    def form_valid(self, form):
        form.save()
        return super(CourseRegistrationView, self).form_valid(form)
    
    def post(self, request, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)
