from django.views.generic import TemplateView
from course.models import Course


class Homepage(TemplateView):
    template_name = "Index.html"
    
    def get_context_data(self, **kwargs):
        _context = super(Homepage, self).get_context_data(**kwargs)
        _context.update({"courses": Course.objects.order_by("?")[:4]})
        return _context
