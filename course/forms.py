from django import forms
from course.models import Course


class CourseForm(forms.ModelForm):
    level_choice = (('Intermediate', 'Intermediate'),
                    ('Advance', 'Advance'),
                    ('Beginners', 'Beginners'),
                    ('Professional', 'Professional',),)
    
    class Meta:
        model = Course
        fields = "__all__"
    
    level = forms.ChoiceField(choices=level_choice)
