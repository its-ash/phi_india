from django.contrib import admin

from reviews.models import Review


@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
    list_display = ('name', 'rating', 'email', 'course', 'verified')
    list_filter = ('date', 'verified',)
    search_fields = ('review', 'name', 'email', 'course')
    readonly_fields = ('id', 'name', 'rating', 'email', 'course', 'date', 'review')
    
    def has_add_permission(self, request):
        return False
