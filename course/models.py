import uuid

from django.db import models
from django.db.models import Avg
from django.utils.text import slugify

from teacher.models import Teachers


class CourseCategory(models.Model):
    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"
    
    id = models.UUIDField(default=uuid.uuid4, primary_key=True)
    name = models.CharField(max_length=100)
    slug = models.SlugField(editable=False)
    
    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        return super(CourseCategory, self).save(*args, **kwargs)
    
    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.__str__()


class Course(models.Model):
    class Meta:
        ordering = ["-created"]
    name = models.CharField(max_length=100)
    basic_cost = models.IntegerField()
    intermediate_cost = models.IntegerField(null=True, blank=True)
    expert_cost = models.IntegerField(null=True, blank=True)
    pdf = models.FileField(upload_to='syllabus_pdf', help_text="Select Syllabus PDF", null=True, blank=True)
    image = models.ImageField(upload_to='course_image', help_text="Select Course Image(400*400)")
    cover = models.ImageField(upload_to='course_cover', help_text="Select Course Cover(1200*900)", null=True, blank=True)
    syllabus = models.TextField(max_length="10000", help_text="Use (.) for nesting")
    teacher = models.ForeignKey(Teachers, on_delete=models.CASCADE, related_name='teacher')
    slug = models.SlugField()
    requirements = models.TextField(max_length=2000)
    learn = models.TextField(max_length=3000, help_text="What Student will Learn", null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    duration = models.IntegerField(default=50)
    lessons = models.IntegerField(default=15)
    level = models.CharField(max_length=100, default="Intermediate")
    category = models.ForeignKey(CourseCategory, on_delete=models.CASCADE, related_name='category')
    summary = models.TextField(max_length=100)
    
    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        return super(Course, self).save(*args, **kwargs)
    
    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.__str__()
    
    @property
    def cost(self):
        return self.basic_cost
    
    @property
    def fake_cost(self):
        return (self.basic_cost * 10) // 6
    
    @property
    def reviews_list(self):
        return self.reviews.filter(verified=True)
    
    @property
    def rating_group(self):
        return self.reviews_list.values('rating').annotate(star=models.Count('rating'))
    
    @property
    def objectives_list(self):
        _objective = self.objectives.all()
        _size = _objective.__len__() // 2
        # create two equal batch for display
        return [_objective[:_size], _objective[_size:]]
    
    @property
    def ratings(self):
        _rating = self.reviews_list.aggregate(rating=Avg('rating')).get('rating')
        return _rating.__round__(1) if _rating else "New Added"
    
    @property
    def ratings_count(self):
        _rating = self.reviews_list.count()
        return _rating if _rating else "No"
    
    @property
    def syllabus_list(self):
        batch = []
        lines = list(filter(lambda x: len(x) > 3, self.syllabus.split("\n")))
        while lines:
            _temp = {'head': lines.pop(0), 'sub_heading': []}
            while lines and lines[0].startswith("*"):
                _temp['sub_heading'].append(lines.pop(0).strip("*"))
            batch.append(_temp)
        return batch


class Objective(models.Model):
    name = models.CharField(max_length=100)
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='objectives')
    
    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.__str__()


class CourseBasicPack(models.Model):
    name = models.CharField(max_length=200)
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='basic')
    
    @property
    def course_name(self):
        return self.course.name
    
    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.__str__()


class CourseIntermediatePack(models.Model):
    name = models.CharField(max_length=100)
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='intermediate')
    
    @property
    def course_name(self):
        return self.course.name
    
    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.__str__()


class CourseExpertPack(models.Model):
    name = models.CharField(max_length=100)
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='expert')
    
    @property
    def course_name(self):
        return self.course.name
    
    def __str__(self):
        return self.name
    
    def __repr__(self):
        return self.__str__()
