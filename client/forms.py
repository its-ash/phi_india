from django import forms
from django.forms import HiddenInput

from client.models import UserData


class UserRegistrationForm(forms.ModelForm):
    class Meta:
        model = UserData
        fields = "__all__"
    
    def __init__(self, *args, **kwargs):
        super(UserRegistrationForm, self).__init__(*args, **kwargs)
        self.fields['course'].widget = HiddenInput()
        self.fields['course_mode'].widget = HiddenInput()
